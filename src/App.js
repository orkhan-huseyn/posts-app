import { useState } from 'react';
import AddPostForm from './AddPostForm';
import PostList from './PostList';

function App() {
  const [posts, setPosts] = useState([]);

  function handleFormSubmit(post) {
    setPosts([...posts, post]);
  }

  function handleDeletePost(indexToDelete) {
    const filteredPosts = posts.filter((_, index) => index !== indexToDelete);
    setPosts(filteredPosts);
  }

  return (
    <div className="container">
      <h1 className="text-center">My posts</h1>
      <AddPostForm onSubmit={handleFormSubmit} />
      <PostList list={posts} onDelete={handleDeletePost} />
    </div>
  );
}

export default App;
